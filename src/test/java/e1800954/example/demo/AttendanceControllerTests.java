package e1800954.example.demo;

import org.apache.commons.collections4.IterableUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

// Student Anouar Belila


@RunWith(SpringRunner.class)
@SpringBootTest
@Configuration
@ComponentScan(basePackages = {"e1800954.example.demo"})
@EnableJpaRepositories(basePackageClasses = AttendanceRepository.class)
public class AttendanceControllerTests {

    @Autowired
    private AttendanceRepository attendanceRepository;

    @Test
    public void postGetDeleteAttendance() {
       Iterable<Attendance> begin = attendanceRepository.findAll();
        System.out.println(IterableUtils.size(begin));
        Attendance att = new Attendance( LocalDate.now(),"ABV");
        Attendance att1 = new Attendance(LocalDate.of(2020,02,02),"JPA");
        System.out.println("ATT : " + att.toString());
        attendanceRepository.save(att);
        attendanceRepository.save(att1);
        long count = attendanceRepository.count();
        System.out.println("SAVED : " + att.toString());
        Attendance found = attendanceRepository.findByKey(att.getKey());
        System.out.println("FOUND : " + att.toString());

        Attendance found1 = attendanceRepository.findByDate(LocalDate.parse("2020-02-02"));
        System.out.println("Found by date is now " + found1.toString() );
        assertThat(found.getKey()).isEqualTo(att.getKey());
        System.out.println("We have " + count + " records");
        Iterable<Attendance> end = attendanceRepository.findAll();
        System.out.println("From the other way we get " + IterableUtils.size(end));

        attendanceRepository.delete(found);


    }



    //when



    // then

}
