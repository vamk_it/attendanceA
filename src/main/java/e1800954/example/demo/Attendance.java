package e1800954.example.demo;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.swing.text.html.Option;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Optional;


@Entity(name = "Attendance")
@Table(name ="attendance" )
@NamedQuery(name = "Attendance.findAll" , query = "SELECT p FROM Attendance p")
public class Attendance implements Serializable {

    public static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(
            name = "attendance_sequence",
            sequenceName = "attendance_sequence",
            allocationSize = 1
    )

    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "attendance_sequence"
    )
    @Column(
            name = "id"
    )
    private int id;

    @Column(
            name = "key"
    )
    private String key;


    @Column(
            name = "date",
            columnDefinition = "TIMESTAMP WITHOUT TIME ZONE",
            nullable = false  // by doing this the application will not save the object if the date is missing (null)
    )
    private LocalDate date;


    public Attendance() {

    }

    /**public Attendance(@JsonProperty("id") int id,
                      @JsonProperty("key") String key) {
        this.id = id;
        this.key = key;

    } **/
    public Attendance(@JsonProperty("date") LocalDate date,
                      @JsonProperty("key") String key) {
        this.date = date;
        this.key = key;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return this.id + "   "+ this.key +"    " + this.date;
    }
}
