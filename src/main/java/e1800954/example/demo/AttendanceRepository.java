package e1800954.example.demo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

@Repository
public interface AttendanceRepository extends JpaRepository<Attendance,Integer> {

    @Transactional
    @Modifying
    @Query("UPDATE Attendance a set a.key = :items WHERE a.id = :id")
    void updateKey(@Param("id") int id,@Param("items") String items);


    public Attendance findByKey(String key);

    public Attendance findByDate(LocalDate date);
}
