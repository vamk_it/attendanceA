package e1800954.example.demo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/attendances")
public class AttendanceController {

    private AttendanceRepository attendanceRepository;

    @Autowired
    public AttendanceController(AttendanceRepository attendanceRepository) {
        this.attendanceRepository = attendanceRepository;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping
    public List<Attendance> list() {
        return attendanceRepository.findAll();
    }

    @PostMapping
    public @ResponseBody Attendance create(@RequestBody Attendance item ) {
        return attendanceRepository.save(item);
    }

    @PutMapping(path = "{id}")
    public @ResponseBody Attendance update(@PathVariable("id")int id,@RequestBody Attendance item) {
        System.out.println(id);
        attendanceRepository.updateKey(id,item.getKey());
        return item;
    }

    @DeleteMapping(path = "{id}")
    public @ResponseBody String delete(@PathVariable("id")int id) {
        attendanceRepository.deleteById(id);
        return "Deleted";
    }
}
